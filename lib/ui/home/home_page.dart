import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather/domain/use_case/get_current_location.dart';
import 'package:weather/domain/use_case/get_forecast.dart';
import 'package:weather/generated/l10n.dart';
import 'package:weather/ui/home/forecast_mode.dart';
import 'package:weather/ui/home/home_cubit.dart';
import 'package:weather/ui/home/home_state.dart';

const _kCelsiusSymbol = '°C';

class HomePage extends StatelessWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) => HomeCubit(
        getCurrentLocation: GetCurrentLocation(
          locationRepository: RepositoryProvider.of(context),
        ),
        getForecast: GetForecast(
          forecastRepository: RepositoryProvider.of(context),
        ),
      )..loadForecast(),
      child: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) => Scaffold(
          appBar: AppBar(
            title: Text(S.of(context).forecastTitle),
            actions: [
              PopupMenuButton(itemBuilder: (context) {
                return [
                  PopupMenuItem<int>(
                    value: 0,
                    child: Text(S.of(context).forecastHourly),
                  ),
                  PopupMenuItem<int>(
                    value: 1,
                    child: Text(S.of(context).forecastDaily),
                  ),
                ];
              }, onSelected: (value) {
                if (value == 0) {
                  context.read<HomeCubit>().setMode(ForecastMode.hourly);
                } else {
                  context.read<HomeCubit>().setMode(ForecastMode.daily);
                }
              }),
            ],
          ),
          body: state.when(
            loading: () => const SizedBox.shrink(),
            loaded: (forecast, mode) => ListView.builder(
              itemCount: mode == ForecastMode.hourly
                  ? forecast.hourly.length
                  : forecast.daily.length,
              itemBuilder: (context, index) => _ForecastItemWidget(
                title: mode == ForecastMode.hourly
                    ? forecast.hourly[index].time.toString()
                    : forecast.daily[index].date.toString(),
                value: mode == ForecastMode.hourly
                    ? _formatTemperature(forecast.hourly[index].temperature)
                    : _formatTemperature(forecast.daily[index].temperature),
                imageUrl: mode == ForecastMode.hourly
                    ? forecast.hourly[index].imageUrl
                    : forecast.daily[index].imageUrl,
              ),
            ),
            error: () => Center(
              child: Text(
                S.of(context).forecastErrorText,
              ),
            ),
          ),
        ),
      ),
    );
  }

  String _formatTemperature(double temperature) {
    return '${temperature.round()}$_kCelsiusSymbol';
  }
}

class _ForecastItemWidget extends StatelessWidget {
  const _ForecastItemWidget({
    Key? key,
    required this.title,
    this.subtitle,
    required this.value,
    required this.imageUrl,
  }) : super(key: key);

  final String title;
  final String? subtitle;
  final String value;
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.displayMedium,
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.displayLarge,
          ),
          CachedNetworkImage(
            height: 64.0,
            width: 64.0,
            imageUrl: imageUrl,
          ),
        ],
      ),
    );
  }
}
