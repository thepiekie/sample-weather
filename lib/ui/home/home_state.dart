import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:weather/domain/forecast.dart';
import 'package:weather/ui/home/forecast_mode.dart';

part 'home_state.freezed.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState.loading() = _Loading;

  const factory HomeState.loaded({
    required Forecast forecast,
    required ForecastMode mode,
  }) = Loaded;

  const factory HomeState.error() = _Error;
}
