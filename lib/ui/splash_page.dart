import 'package:flutter/material.dart';
import 'package:weather/generated/l10n.dart';
import 'package:weather/ui/constants.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Future.delayed(const Duration(
        seconds: kSplashScreenDurationSeconds,
      )),
      builder: (context, snapshot) =>
          snapshot.connectionState == ConnectionState.done
              ? child
              : Scaffold(
                  body: Center(
                    child: Text(S.of(context).splash),
                  ),
                ),
    );
  }
}
