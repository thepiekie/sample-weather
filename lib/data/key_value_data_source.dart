import 'package:shared_preferences/shared_preferences.dart';

abstract class KeyValueDataSource {
  Future write({
    required String key,
    required String value,
  });

  Future<String?> read({
    required String key,
  });
}

class SharedPreferencesDataSource implements KeyValueDataSource {
  SharedPreferencesDataSource({
    required SharedPreferences sharedPreferences,
  }) : _sharedPreferences = sharedPreferences;

  final SharedPreferences _sharedPreferences;

  @override
  Future<String?> read({
    required String key,
  }) async {
    return _sharedPreferences.getString(key);
  }

  @override
  Future write({
    required String key,
    required String value,
  }) async {
    _sharedPreferences.setString(key, value);
  }
}
