import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:weather/data/constants.dart';
import 'package:weather/data/forecast/forecast_entity.dart';

part 'forecast_api.g.dart';

@RestApi(baseUrl: kOpenWeatherMapApiUrl)
abstract class ForecastApi {
  factory ForecastApi(
    Dio dio, {
    String baseUrl,
  }) = _ForecastApi;

  @GET("/onecall")
  Future<ForecastEntity> getForecast(
    @Query("lat") double lat,
    @Query("lon") double lon,
    @Query("exclude") String? exclude,
    @Query("units") String? units,
  );
}
