import 'dart:convert';

import 'package:weather/data/constants.dart';
import 'package:weather/data/forecast/forecast_api.dart';
import 'package:weather/data/forecast/forecast_entity.dart';
import 'package:weather/data/key_value_data_source.dart';

abstract class ForecastDataSource {
  Future<ForecastEntity?> getForecast({
    required double lat,
    required double lon,
  });
}

class ForecastLocalDataSource implements ForecastDataSource {
  ForecastLocalDataSource({
    required KeyValueDataSource keyValueDataSource,
  }) : _keyValueDataSource = keyValueDataSource;

  final KeyValueDataSource _keyValueDataSource;

  @override
  Future<ForecastEntity?> getForecast({
    required double lat,
    required double lon,
  }) async {
    final storedValue = await _keyValueDataSource.read(
      key: '$kForecastLocalKeyPrefix$lat$lon',
    );
    if (storedValue != null) {
      return ForecastEntity.fromJson(jsonDecode(storedValue));
    } else {
      return null;
    }
  }
}

class ForecastApiDataSource implements ForecastDataSource {
  ForecastApiDataSource({
    required ForecastApi forecastApi,
  }) : _forecastApi = forecastApi;

  final ForecastApi _forecastApi;

  @override
  Future<ForecastEntity> getForecast({
    required double lat,
    required double lon,
  }) {
    return _forecastApi.getForecast(
      lat,
      lon,
      kForecastApiExclude,
      kForecastApiUnits,
    );
  }
}
