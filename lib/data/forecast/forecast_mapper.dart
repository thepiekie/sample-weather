import 'package:intl/intl.dart';
import 'package:weather/data/constants.dart';
import 'package:weather/data/forecast/forecast_entity.dart';
import 'package:weather/domain/forecast.dart';

class ForecastMapper {
  final DateFormat _dateFormatter = DateFormat('dd.MM');

  Forecast mapForecastEntity(ForecastEntity entity) {
    return Forecast(
      hourly: entity.hourly
          .map((e) => HourlyForecast(
                time:
                    '${DateTime.fromMillisecondsSinceEpoch(e.datetime * 1000).hour}:00',
                temperature: e.temperature,
                imageUrl: kOpenWeatherImageUrl +
                    e.weather.first.icon +
                    kOpenWeatherImageFormat,
              ))
          .toList(),
      daily: entity.daily
          .map(
            (e) => DailyForecast(
              date: _dateFormatter.format(
                  DateTime.fromMillisecondsSinceEpoch(e.datetime * 1000)),
              temperature: e.temperature.dayTemperature,
              imageUrl: kOpenWeatherImageUrl +
                  e.weather.first.icon +
                  kOpenWeatherImageFormat,
            ),
          )
          .toList(),
    );
  }
}
