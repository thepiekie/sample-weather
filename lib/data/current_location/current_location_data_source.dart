import 'dart:convert';

import 'package:weather/data/constants.dart';
import 'package:weather/data/current_location/current_location_entity.dart';
import 'package:weather/data/key_value_data_source.dart';

abstract class CurrentLocationDataSource {
  Future<CurrentLocationEntity?> getLocation();

  Future<void> saveLocation(
    CurrentLocationEntity location,
  );
}

class CurrentLocationLocalDataSource implements CurrentLocationDataSource {
  CurrentLocationLocalDataSource({
    required KeyValueDataSource keyValueDataSource,
  }) : _keyValueDataSource = keyValueDataSource;

  final KeyValueDataSource _keyValueDataSource;

  @override
  Future<CurrentLocationEntity?> getLocation() async {
    final storedValue = await _keyValueDataSource.read(
      key: kLocationLocalKeyPrefix,
    );
    if (storedValue != null) {
      return CurrentLocationEntity.fromJson(jsonDecode(storedValue));
    }
    return null;
  }

  @override
  Future<void> saveLocation(CurrentLocationEntity location) {
    final json = jsonEncode(location.toJson());
    return _keyValueDataSource.write(
      key: kLocationLocalKeyPrefix,
      value: json,
    );
  }
}
