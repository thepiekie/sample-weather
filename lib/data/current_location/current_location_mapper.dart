import 'package:weather/data/current_location/current_location_entity.dart';
import 'package:weather/domain/current_location.dart';

class CurrentLocationMapper {
  CurrentLocation? mapEntityToModel(CurrentLocationEntity? entity) {
    if (entity != null) {
      return CurrentLocation(
        lat: entity.latitude,
        lon: entity.longitude,
      );
    }
    return null;
  }

  CurrentLocationEntity mapModelToEntity(CurrentLocation entity) {
    return CurrentLocationEntity(
      entity.lat,
      entity.lon,
    );
  }
}
