import 'package:weather/data/current_location/current_location_data_source.dart';
import 'package:weather/data/current_location/current_location_mapper.dart';
import 'package:weather/domain/current_location.dart';

abstract class CurrentLocationRepository {
  Future<CurrentLocation?> getLocation();

  Future<void> saveLocation(
    CurrentLocation location,
  );
}

class DefaultCurrentLocationRepository implements CurrentLocationRepository {
  DefaultCurrentLocationRepository({
    required CurrentLocationDataSource localDataSource,
    required CurrentLocationMapper mapper,
  })  : _localDataSource = localDataSource,
        _mapper = mapper;

  final CurrentLocationDataSource _localDataSource;
  final CurrentLocationMapper _mapper;

  @override
  Future<CurrentLocation?> getLocation() {
    return _localDataSource
        .getLocation()
        .then((value) => _mapper.mapEntityToModel(value));
  }

  @override
  Future<void> saveLocation(CurrentLocation location) {
    return _localDataSource.saveLocation(_mapper.mapModelToEntity(location));
  }
}
