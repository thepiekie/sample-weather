import 'package:geolocator/geolocator.dart';
import 'package:weather/data/current_location/current_location_repository.dart';
import 'package:weather/domain/current_location.dart';
import 'package:weather/domain/use_case/use_case.dart';

/// Allows to fetch current user location.
/// If all the necessary permissions were not granted
/// then returns position that is stored.
class GetCurrentLocation extends UseCase<CurrentLocation?, void> {
  GetCurrentLocation({
    required CurrentLocationRepository locationRepository,
  }) : _locationRepository = locationRepository;

  final CurrentLocationRepository _locationRepository;

  @override
  Future<CurrentLocation?> call([void parameters]) {
    return _permissionsGranted().then((granted) {
      if (granted) {
        return Geolocator.getCurrentPosition().then((value) async {
          // We should store location, then return
          final location = CurrentLocation(
            lat: value.latitude,
            lon: value.longitude,
          );
          await _locationRepository.saveLocation(location);
          return location;
        });
      } else {
        return _locationRepository.getLocation();
      }
    });
  }

  Future<bool> _permissionsGranted() async {
    final serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return false;
    }
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return false;
    }
    return true;
  }
}
