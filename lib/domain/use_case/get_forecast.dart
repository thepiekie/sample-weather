import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:weather/data/forecast/forecast_repository.dart';
import 'package:weather/domain/constants.dart';
import 'package:weather/domain/forecast.dart';
import 'package:weather/domain/use_case/use_case.dart';

part 'get_forecast.freezed.dart';

@freezed
class GetForecastParameters with _$GetForecastParameters {
  const factory GetForecastParameters({
    @Default(kGetForecastDefaultLat) double latitude,
    @Default(kGetForecastDefaultLon) double longitude,
  }) = _GetForecastParameters;
}

/// Returns forecast for given latitude, longitude.
/// Note: [GetForecastParameters] has default values.
class GetForecast implements UseCase<Forecast, GetForecastParameters> {
  GetForecast({
    required ForecastRepository forecastRepository,
  }) : _forecastRepository = forecastRepository;

  final ForecastRepository _forecastRepository;

  @override
  Future<Forecast> call([GetForecastParameters? parameters]) async {
    assert(parameters != null, 'GetForecastParameters cannot be null');
    return _forecastRepository.getForecast(
      lat: parameters!.latitude,
      lon: parameters.longitude,
    );
  }
}
