import 'package:freezed_annotation/freezed_annotation.dart';

part 'current_location.freezed.dart';

@freezed
class CurrentLocation with _$CurrentLocation {
  const factory CurrentLocation({
    required double lat,
    required double lon,
  }) = _CurrentLocation;
}
