enum ErrorType {
  locationServiceDisabled,
  locationPermissionDenied,
  locationPermissionDeniedForever,
}
